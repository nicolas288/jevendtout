package com.nextu.service;

import com.nextu.dto.CategoryDTO;
import com.nextu.entity.Category;
import com.nextu.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryService {
    private final CategoryMapper categoryMapper;
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryMapper categoryMapper, CategoryRepository categoryRepository) {
        this.categoryMapper = categoryMapper;
        this.categoryRepository = categoryRepository;
    }

    public Optional<CategoryDTO> get(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        return category.map(categoryMapper::toDTO);
    }

    public List<Optional<CategoryDTO>> getAll() {
        List<Optional<CategoryDTO>> categoryDTOList = new ArrayList<>();
        List<Category> categoryList = categoryRepository.findAll();
        for (Category category : categoryList) {
            categoryDTOList.add(Optional.ofNullable(categoryMapper.toDTO(category)));
        }
        return categoryDTOList;
    }
}
