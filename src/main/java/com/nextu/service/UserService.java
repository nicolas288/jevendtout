package com.nextu.service;

import com.nextu.dto.UserDTO;
import com.nextu.entity.User;
import com.nextu.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserService {
    private final UserMapper userMapper;
    private final UserRepository userRepository;

    public UserService(UserMapper userMapper, UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    public Optional<UserDTO> get(Long id) {
        Optional<User> user = userRepository.findById(id);
        return user.map(userMapper::toDTO);
    }

    public List<Optional<UserDTO>> getAll() {
        List<Optional<UserDTO>> userDTOList = new ArrayList<>();
        List<User> userList = userRepository.findAll();
        for (User user : userList) {
            userDTOList.add(Optional.ofNullable(userMapper.toDTO(user)));
        }
        return userDTOList;
    }

}
