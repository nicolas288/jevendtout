package com.nextu.service;

import com.nextu.dto.ProductLineDTO;
import com.nextu.entity.ProductLine;
import com.nextu.repository.ProductLineRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductLineService {
    private final ProductLineMapper productLineMapper;
    private final ProductLineRepository productLineRepository;

    public ProductLineService(ProductLineMapper productLineMapper, ProductLineRepository productLineRepository) {
        this.productLineMapper = productLineMapper;
        this.productLineRepository = productLineRepository;
    }

    public Optional<ProductLineDTO> get(Long id) {
        Optional<ProductLine> productLine = productLineRepository.findById(id);
        return category.map(productLineMapper::toDTO);
    }

    public List<Optional<ProductLineDTO>> getAll() {
        List<Optional<ProductLineDTO>> productLineDTOList = new ArrayList<>();
        List<ProductLine> productLineList = productLineRepository.findAll();
        for (ProductLine productLine : productLineList) {
            productLineDTOList.add(Optional.ofNullable(productLineMapper.toDTO(productLine)));
        }
        return productLineDTOList;
    }
}
