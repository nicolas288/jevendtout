package com.nextu.service;

import com.nextu.dto.ProductLineDTO;
import com.nextu.entity.ProductLine;

public class ProductLineMapper {
    public ProductLineDTO toDTO(ProductLine productLine) {
        ProductLineDTO productLineDTO = new ProductLineDTO();
        productLineDTO.setProduct(productLine.getProduct());
        productLineDTO.setQuantity(productLine.getQuantity());
        productLineDTO.setTotal(productLine.getTotal());
        return productLineDTO;
    }

    public ProductLine toEntity(ProductLineDTO productLineDTO) {
        ProductLine productLine = new ProductLine();
        productLine.setProduct(ProductLineDTO.getProduct());
        productLine.setQuantity(ProductLineDTO.getQuantity());
        productLine.setTotal(ProductLineDTO.getTotal());
        return category;
    }

}
