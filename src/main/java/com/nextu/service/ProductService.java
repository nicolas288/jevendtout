package com.nextu.service;

import com.nextu.dto.ProductDTO;
import com.nextu.entity.Product;
import com.nextu.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductService {
    private final ProductMapper productMapper;
    private final ProductRepository productRepository;

    public ProductService(ProductMapper productMapper, ProductRepository productRepository) {
        this.productMapper = productMapper;
        this.productRepository = productRepository;
    }

    public Optional<ProductDTO> get(Long id) {
        Optional<Product> product = productRepository.findById(id);
        return product.map(productMapper::toDTO);
    }

    public List<Optional<ProductDTO>> getAll() {
        List<Optional<ProductDTO>> productDTOList = new ArrayList<>();
        List<Product> productList = productRepository.findAll();
        for (Product product : productList) {
            productDTOList.add(Optional.ofNullable(productMapper.toDTO(product)));
        }
        return productDTOList;
    }

    public List<ProductDTO> findAllByCategory(Long categoryId) {
        List<ProductDTO> productDTOList = new ArrayList<>();
        List<Product> productList = productRepository.findAllByCategory(categoryId);
        for (Product product : productList) {
            productDTOList.add(productMapper.toDTO(product));
        }
        return productDTOList;
    }

}
