package com.nextu.repository;

import com.nextu.entity.Category;
import com.nextu.entity.ProductLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductLineRepository extends JpaRepository<ProductLine, Long> {
}
