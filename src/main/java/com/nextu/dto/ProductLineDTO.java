package com.nextu.dto;

import com.nextu.entity.Product;
import jakarta.persistence.*;

import java.util.Objects;

public class ProductLineDTO {
    private Long id;
    private Product product;
    private Long quantity;
    private Double total;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductLineDTO)) return false;
        ProductLineDTO that = (ProductLineDTO) o;
        return Objects.equals(product, that.product) && Objects.equals(quantity, that.quantity) && Objects.equals(total, that.total);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, quantity, total);
    }

    @Override
    public String toString() {
        return "ProductLine{" +
                "product=" + product +
                ", quantity=" + quantity +
                ", total=" + total +
                '}';
    }
}
