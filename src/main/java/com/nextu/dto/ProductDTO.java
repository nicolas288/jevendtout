package com.nextu.dto;

import com.nextu.entity.Category;

public class ProductDTO {
    private Long id;
    private String name;
    private Double price;
    private Category category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Category getCategory_id() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
