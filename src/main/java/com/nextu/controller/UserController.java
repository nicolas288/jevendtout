package com.nextu.controller;

import com.nextu.dto.UserDTO;
import com.nextu.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user/{id}")
    public Optional<UserDTO> item(@PathVariable Long id) {
        return userService.get(id);
    }

    @GetMapping("/users")
    public List<Optional<UserDTO>> list() {
        return userService.getAll();
    }
}
