package com.nextu.controller;

import com.nextu.dto.ProductDTO;
import com.nextu.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/{id}")
    public Optional<ProductDTO> item(@PathVariable Long id) {
        return productService.get(id);
    }

    @GetMapping("/products")
    public List<Optional<ProductDTO>> list() {
        return productService.getAll();
    }

    @GetMapping("/category/{id}/products")
    public List<ProductDTO> categoryProducts(@PathVariable Long id) { return productService.findAllByCategory(id); }
}
