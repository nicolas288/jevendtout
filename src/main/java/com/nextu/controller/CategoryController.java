package com.nextu.controller;

import com.nextu.dto.CategoryDTO;
import com.nextu.service.CategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category/{id}")
    public Optional<CategoryDTO> item(@PathVariable Long id) {
        return categoryService.get(id);
    }

    @GetMapping("/categories")
    public List<Optional<CategoryDTO>> list() {
        return categoryService.getAll();
    }
}
